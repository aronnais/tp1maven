package fr.istic.master1.sir;

public class Personne 
{ 
    private String nom; 
    private String prenom; 
    private int age; 
    private String test = "manu";

    /**
     * Objet personne
     * @param n le nom
     * @param p le prénom
     * @param a l'age
     */
    public Personne(String n, String p, int a) {
        this.nom = n; 
        this.prenom = p; 
        this.age = a; 
    } 

    public String coucou() {
        if(false)
            System.out.println("pas coucou");
        return "coucou";
    }
    
    
    /**
     * Fonction pour afficher la personne
     */
    public void afficher() {
        System.out.println("Nom : " + nom + " prenom : " + prenom + " age : " + age);
        if(this.age > 10) {
            System.out.println("je suis grand");
        } else {
            
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
    }
    
    /**
     * Fonction pour afficher la personne
     */
    public void afficher2() {
        System.out.println("Nom : " + nom + " prenom : " + prenom + " age : " + age);
        if(this.age > 10) {
            System.out.println("je suis grand");
        } else {
            
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
        if(this.age > 10) {
            System.out.println("je suis grand");
        }
    }


    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getPrenom() {
        return prenom;
    }


    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    } 
    
}